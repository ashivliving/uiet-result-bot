from selenium import webdriver
import sys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import time

total = len(sys.argv)
if total != 3:
    print "\nPass 2 argument i.e. - RollNo. and semester\n"
else:
    print "Getting your Result - :p "
    a = sys.argv[1]
    if len(a) == 1:
        roll = sys.argv[2]
        sem = sys.argv[1]
    else:
        roll = sys.argv[1]
        sem = sys.argv[2]
    roll = str(roll).upper()
    #print roll.upper()
    driver = webdriver.PhantomJS(service_args=['--ignore-ssl-errors=true'])
    driver.get('https://uwp.puchd.ac.in/common/viewmarks.aspx')
    driver.maximize_window()
    #driver.find_element_by_xpath('//*[@id="toolbar"]/div/div/ul/li[2]/a').click()
    #driver.find_element_by_xpath('/html/body/center/h1[4]/a').click()
    #driver.find_element_by_xpath('//*[@id="form1"]/div[3]/center/table/tbody/tr[2]/td[1]/a[1]').click()
    driver.find_element_by_id('ctl00_middleContent_TextBox1').clear()
    driver.find_element_by_id('ctl00_middleContent_TextBox1').send_keys(roll)
    Select(driver.find_element_by_id("ctl00_middleContent_DropDownList1")).select_by_visible_text(str(sem))
    driver.find_element_by_id('ctl00_middleContent_cmdsubmit').click()
    time.sleep(2)
    try:
        print driver.find_element_by_id('ctl00_middleContent_Label3').text
        print driver.find_element_by_id('ctl00_middleContent_Label7').text
    except NoSuchElementException:
        print "Sorry! Your "+str(sem)+ " sem result is not available."
